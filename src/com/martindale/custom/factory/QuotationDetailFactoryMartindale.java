package com.martindale.custom.factory;

import java.util.List;

import com.martindale.custom.server.bo.QuotationDetailMartindale;
import com.netappsid.erp.server.bo.QuotationDetail;
import com.netappsid.erp.server.bo.TransactionDetail;
import com.netappsid.erp.server.interfaces.TransactionDetailFactory;
import com.netappsid.factory.utils.FactoryUtils;

public class QuotationDetailFactoryMartindale extends com.netappsid.erp.server.factory.QuotationDetailFactory implements
		TransactionDetailFactory<QuotationDetail>
{
	@Override
	public QuotationDetail create()
	{
		return (QuotationDetailMartindale) FactoryUtils.getBOEntity(QuotationDetailMartindale.class);
	}

	@Override
	public QuotationDetail clone(TransactionDetail detail, List<String> fieldExclude, boolean keepDiscount)
	{
		QuotationDetailMartindale cloneQuotationDetail = (QuotationDetailMartindale) super.clone(detail, fieldExclude, keepDiscount);

		return cloneQuotationDetail;
	}
}
